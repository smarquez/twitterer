class Account < ApplicationRecord
  has_many :posts
  has_many :follows, class_name: 'Follow', foreign_key: 'follower_id'

  validates_presence_of :name
end
