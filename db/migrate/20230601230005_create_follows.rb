class CreateFollows < ActiveRecord::Migration[7.0]
  def change
    create_table :follows do |t|
      t.string :content
      t.belongs_to :follower, null: false, foreign_key: { to_table: :accounts }
      t.belongs_to :followed, null: false, foreign_key: { to_table: :accounts }

      t.timestamps
    end
  end
end
