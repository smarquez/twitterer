require 'rails_helper'

RSpec.describe '/accounts', type: :request do
  let(:valid_attributes) do
    { name: 'My account' }
  end

  let(:invalid_attributes) do
    { age: 12 }
  end

  let(:valid_headers) do
    {}
  end

  describe 'GET /index' do
    it 'renders a successful response' do
      Account.create! valid_attributes
      get accounts_url, headers: valid_headers, as: :json
      expect(response).to be_successful
    end
  end

  describe 'GET /show' do
    it 'renders a successful response' do
      account = Account.create! valid_attributes
      get account_url(account), as: :json
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new Account' do
        expect do
          post accounts_url,
               params: { account: valid_attributes }, headers: valid_headers, as: :json
        end.to change(Account, :count).by(1)
      end

      it 'renders a JSON response with the new account' do
        post accounts_url,
             params: { account: valid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:created)
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new Account' do
        expect do
          post accounts_url,
               params: { account: invalid_attributes }, as: :json
        end.to change(Account, :count).by(0)
      end

      it 'renders a JSON response with errors for the new account' do
        post accounts_url,
             params: { account: invalid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested account' do
      account = Account.create! valid_attributes
      expect do
        delete account_url(account), headers: valid_headers, as: :json
      end.to change(Account, :count).by(-1)
    end
  end
end
