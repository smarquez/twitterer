require 'rails_helper'

RSpec.describe 'Follows', type: :request do
  let(:account_1) { Account.create! name: :account_1 }
  let(:account_2) { Account.create! name: :account_2 }

  let(:valid_attributes) do
    { follower_id: account_1.id, followed_id: account_2.id }
  end

  let(:invalid_attributes) do
    { follower_id: nil, followed_id: account_2.id }
  end

  let(:valid_headers) do
    {}
  end

  describe 'POST /create' do
    it 'create a follow' do
      expect do
        post follows_url,
             params: { follow: valid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:success)
      end.to change(Follow, :count).by(1)
    end

    it 'renders a JSON response with the new follow' do
      post follows_url,
           params: { follow: valid_attributes }, headers: valid_headers, as: :json
      expect(response).to have_http_status(:created)
      expect(response.content_type).to match(a_string_including('application/json'))
    end
  end

  context 'with invalid parameters' do
    it 'does not create a new Follow' do
      expect do
        post follows_url,
             params: { follow: invalid_attributes }, as: :json
      end.to change(Follow, :count).by(0)
    end

    it 'renders a JSON response with errors for the new follow' do
      post follows_url,
           params: { follow: invalid_attributes }, headers: valid_headers, as: :json
      expect(response).to have_http_status(:unprocessable_entity)
      expect(response.content_type).to match(a_string_including('application/json'))
    end
  end
end
