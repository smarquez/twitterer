require 'rails_helper'

RSpec.describe '/posts', type: :request do
  let(:account_1) { Account.create name: 'account_1' }
  let(:account_2) { Account.create name: 'account_2' }

  let(:valid_attributes) do
    { content: 'some content', account_id: account_1.id }
  end

  let(:invalid_attributes) do
    { age: 'Mr tweet' }
  end

  let(:valid_headers) do
    {}
  end

  describe 'GET /index' do
    before do
      Post.create! content: 'some content 1', account_id: account_1.id
      Post.create! content: 'some content 2', account_id: account_1.id
      Post.create! content: 'some content 3', account_id: account_1.id
      Post.create! content: 'some content 4', account_id: account_2.id
      Post.create! content: 'some content 5', account_id: account_2.id
      Post.create! content: 'some content 6', account_id: account_2.id
      Follow.create! follower_id: account_1.id, followed_id: account_2.id
    end

    it 'renders a successful response' do
      get posts_url(account_id: account_1), headers: valid_headers, as: :json
      expect(response).to be_successful
    end

    it 'filters posts by account' do
      get posts_url(account_id: account_1), headers: valid_headers, as: :json
      body = JSON.parse response.body
      expect(body.count).to be(3)
    end
  end

  describe 'GET /show' do
    it 'renders a successful response' do
      post = Post.create! valid_attributes
      get post_url(post), as: :json
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new Post' do
        expect do
          post posts_url,
               params: { post: valid_attributes }, headers: valid_headers, as: :json
        end.to change(Post, :count).by(1)
      end

      it 'renders a JSON response with the new post' do
        post posts_url,
             params: { post: valid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:created)
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new Post' do
        expect do
          post posts_url,
               params: { post: invalid_attributes }, as: :json
        end.to change(Post, :count).by(0)
      end

      it 'renders a JSON response with errors for the new post' do
        post posts_url,
             params: { post: invalid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested post' do
      post = Post.create! valid_attributes
      expect do
        delete post_url(post), headers: valid_headers, as: :json
      end.to change(Post, :count).by(-1)
    end
  end
end
