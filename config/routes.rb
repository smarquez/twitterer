Rails.application.routes.draw do
  resources :posts
  resources :accounts
  resources :follows

  # Defines the root path route ("/")
  root 'accounts#index'
end
